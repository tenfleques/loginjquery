var g__loggedin = false;
var g__lang = "ru" // can be "en"
//under normal circumstances would use storage services for a token
jQuery.fn.extend({
  ajaxApi : function(api, payload){//расшерение $.post
    if(typeof(arguments[2])=="function")//callback
      arguments[2].apply(this);
    var req = $.ajax({
      url: api,
      method: "POST",
      data: payload,
      dataType: "json"
    })
    return req;
  },
	authenticated : function(){
		if (!g__loggedin)
			$().loginprompt() // закрыват все виды кроме аутентификация
		else {
      $("#login-now-modal").modal("hide"); // заблокировать логтн промпт
		}
	},
	loginprompt : function(){
		$("#login-now-modal").modal({
			show : true,
			keyboard: false,
			focus: true,
			backdrop:'static'
		})//modal статическая и закрывается толко кодом
	},
	loginCtrl: function(){
		$("body").on("submit","#authentication",function(e){
			e.preventDefault();
      //var auth = $(this).serialize() //пробовал через serialize() но получил 442 также
      var auth = {"authentication": {
          "login": $("#authentication_login").val(),
          "password": $("#authentication_password").val()
        }
      }
			var req =  $().ajaxApi("https://api.vs61.nwaj.ru/v1/user/authentication",auth);
			req.done(function(data){
				g__loggedin = true; //в production конечно быть бы token
        $(".profile").removeClass("hidden"); //открывать профиль
				$("#login-signup-alert-fail").addClass("hidden"); //закрыть сообщение об ощибке
			});
			req.fail(function(e){
				$("#login-signup-alert-fail").html("логин/пароль не верно");//загрузить сообщение об ощибке
				$("#login-signup-alert-fail").removeClass("hidden"); // открывать ощибку
        $(".profile").addClass("hidden");//закрывать профиль
			})
		})
	},
	localize : function(){ //в случае пользования много языков
		$(".select-langs").val(g__lang);
		$(".multi-lang").css("display","none");
		$("."+g__lang).css("display","block");
	}
});
$(function(){
	$().authenticated(); // запросить если пользователь допустно и продолжать в профиль иначе покажет аутентификацию
	$().loginCtrl(); // зарегиструй событие субмит форму
});
